//SPDX-License-Identifier: MIT

pragma solidity 0.8.6;

contract enum1{

    //int my_num = 34;

    enum mylist{apple,laptop,cellphone}
    mylist item;

    function setCellphone() public
    {
        item = mylist.cellphone;
    }
    
    function getItem() public view returns (mylist)
    {
        return item;
    }

}

//SPDX-License-Identifier: MIT

pragma solidity 0.8.6;

contract structure{

    struct Book{
        string title;
        int price;
        string author;
        int YOP;
    }
    Book book;

    function setBook() public{
        book=Book("NodeJS",79,"Sarani",2022);
    }

    function getBook() public view returns (int)
    {
        return book.price;
    }
}
    

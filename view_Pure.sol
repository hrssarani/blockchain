//SPDX-License-Identifier: MIT

pragma solidity 0.8.6;

//Objected oriented programming (a way to  implement abstraction)
//view function declares that no state will be changed (partial abstraction)
//pure functino declared that no state will be changed or read (compelete abstraction)

contract viewPure{

    //internal
    int public x =1;

    function addX(int y) public view returns (int){
        
//we are able to read the vlaue of x

        return x+y;
    }

//niether read nor modified
    function AddX(int i, int j) public pure returns (int){
        int d=i+j;
        return d;
    }
}

//deploy using 0x53b000 - this accout stores byte corde of contact 

//call the function using this address 0xAb8483F64d9C6d1EcF9b849Ae677dD3315835cb2
